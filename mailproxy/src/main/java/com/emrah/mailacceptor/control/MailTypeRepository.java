package com.emrah.mailacceptor.control;


import com.emrah.mailacceptor.entity.MailType;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;



/**
 * Created by emrahkucukkaraca on 9.09.2018.
 */
@Stateless public class MailTypeRepository {

    @PersistenceContext
    EntityManager entityManager;


    public List<MailType> findAll() {
        return entityManager.createNamedQuery(MailType.FIND_ALL, MailType.class).getResultList();
    }

    public MailType findByCode(String code) {
        try {
            return entityManager.createNamedQuery(MailType.FIND_BY_CODE, MailType.class)
                .setParameter("code", code).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public MailType save(MailType mailType) {
        MailType byCode = findByCode(mailType.getCode());
        if (byCode == null) {
            return entityManager.merge(mailType);
        }
        return null;


    }
}
