package com.emrah.mailacceptor.control;

import com.emrah.mailacceptor.entity.Mail;
import com.emrah.mailacceptor.entity.MailType;
import com.emrah.mailsender.entity.MailStatus;
import com.emrah.mailsender.entity.ValidMail;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * Created by emrahkucukkaraca on 11.09.2018.
 */
public class PrepareMail {

    @Inject
    MailTypeRepository mailTypeRepository;


    public ValidMail prepare(final Mail mail) {
        ValidMail validMail = new ValidMail();
        MailType byCode = mailTypeRepository.findByCode(mail.getMailTypeCode());
        validMail.setBody(fillParameters(byCode.getTemplate(), mail.getParamaters()));
        validMail.setFrom(byCode.getFrom());
        validMail.setRetryCount((short) 0);
        validMail.setStatus(MailStatus.NOT_SEND);
        validMail.setSubject(fillParameters(byCode.getSubject(), mail.getParamaters()));
        if(mail.getTimeToSend()!=null){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime dateTime = LocalDateTime.parse(mail.getTimeToSend(), formatter);
            validMail.setTimeToSend(dateTime);
        }

        validMail.setTo(mail.getTo());
        return validMail;
    }

    private String fillParameters(String text, Map<String, String> parameters) {
        parameters.keySet().forEach(param -> text.replaceAll(param, parameters.get(param)));
        return text;

    }
}
