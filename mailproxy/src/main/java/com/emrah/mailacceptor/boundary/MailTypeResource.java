package com.emrah.mailacceptor.boundary;

import com.emrah.mailacceptor.control.MailTypeRepository;
import com.emrah.mailacceptor.entity.MailType;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

/**
 * Created by emrahkucukkaraca on 9.09.2018.
 */

@Path("mail-types")
@Produces("application/json")
@Consumes("application/json")
public class MailTypeResource {

    @Inject
    MailTypeRepository mailTypeRepository;

    @GET
    public List allMailTypes() {
        return mailTypeRepository.findAll();
    }

    @GET
    @Path("{code}")
    public Response mailType(@PathParam("code") String code) {
        MailType byCode = mailTypeRepository.findByCode(code);
        if (byCode != null) {
            return Response.ok(byCode).build();
        }
        return Response.noContent().header("Error", "There is no mail type with the code " + code)
            .build();
    }


    @POST
    public Response createMailType(@Valid MailType mailType, @Context UriInfo info) {
        MailType saved = mailTypeRepository.save(mailType);
        if (saved != null) {
            URI uri = info.getAbsolutePathBuilder().path("/" + saved.getCode()).build();
            return Response.created(uri).build();
        }
        return Response.status(Status.BAD_REQUEST).header("Error",
            "There is a mailtype with the code " + mailType.getCode()
                + " please select another one").build();
    }
}
