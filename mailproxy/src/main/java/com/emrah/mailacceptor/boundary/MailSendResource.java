package com.emrah.mailacceptor.boundary;

import com.emrah.mailacceptor.control.PrepareMail;
import com.emrah.mailacceptor.entity.Mail;
import com.emrah.mailsender.control.ValidMailRepository;
import com.emrah.mailsender.entity.ValidMail;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by emrahkucukkaraca on 11.09.2018.
 */

@Path("mails")
@Produces("application/json")
@Consumes("application/json")
public class MailSendResource {

    @Inject
    PrepareMail prepareMail;

    @Inject
    ValidMailRepository validMailRepository;

    @POST
    public Long sendMail(@Valid Mail mail) {
        ValidMail validMail = prepareMail.prepare(mail);
        ValidMail save = validMailRepository.save(validMail);
        return save.getId();

    }

    @GET
    @Path("{id}")
    public Response validMail(@PathParam("id") Long id) {
        ValidMail validMail = validMailRepository.findById(id);
        if (validMail.getId() != null) {
            return Response.ok(validMail).build();
        }
        return Response.noContent().build();
    }


}
