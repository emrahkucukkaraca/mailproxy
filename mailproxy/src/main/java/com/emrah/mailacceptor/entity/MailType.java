package com.emrah.mailacceptor.entity;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static com.emrah.mailacceptor.entity.MailType.FIND_ALL;
import static com.emrah.mailacceptor.entity.MailType.FIND_BY_CODE;

/**
 * Created by emrahkucukkaraca on 9.09.2018.
 */
@Entity
@Table(name = "MAIL_TYPES", indexes = {
    @Index(name = "MAIL_TYPE_CODE", columnList = "CODE", unique = true)
})
@NamedQueries({
    @NamedQuery(name = FIND_ALL, query = "SELECT mt FROM MailType mt"),
    @NamedQuery(name = FIND_BY_CODE, query = "SELECT mt FROM MailType mt  WHERE mt.code=:code")
})
public class MailType implements Serializable {

    public static final String PREFIX = "MailType";
    public static final String FIND_ALL = PREFIX + ".FindAll";
    public static final String FIND_BY_CODE = PREFIX + ".FindByCode";

    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    @NotNull
    private String code;
    private String name;
    @Email
    @Column(name = "from_address")
    private String from;
    @NotNull
    private String subject;

    @NotNull
    @Column(columnDefinition = "VARCHAR(4000)")
    private String template;
    @NotEmpty
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> parameters = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Set<String> getParameters() {
        return parameters;
    }

    public void setParameters(final Set<String> parameters) {
        this.parameters = parameters;
    }
}
