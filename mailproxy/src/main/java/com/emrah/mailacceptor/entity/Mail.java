package com.emrah.mailacceptor.entity;

import com.emrah.validation.ValidDateTime;
import com.emrah.validation.ValidMailModel;
import com.emrah.validation.ValidMailParameter;
import com.emrah.validation.ValidMailType;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by emrahkucukkaraca on 9.09.2018.
 */
@ValidMailParameter
public class Mail implements Serializable, ValidMailModel {

    @ValidMailType
    private String mailTypeCode;
    @NotNull
    @Email
    private String to;
    @NotEmpty
    private Map<String, String> paramaters = new HashMap<>();
    @ValidDateTime
    private String timeToSend;


    @Override
    public String getMailTypeCode() {
        return mailTypeCode;
    }

    public void setMailTypeCode(String mailTypeCode) {
        this.mailTypeCode = mailTypeCode;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Map<String, String> getParamaters() {
        return paramaters;
    }

    public void setParamaters(Map<String, String> paramaters) {
        this.paramaters = paramaters;
    }

    public String getTimeToSend() {
        return timeToSend;
    }

    public void setTimeToSend(final String timeToSend) {
        this.timeToSend = timeToSend;
    }

    @Override
    public boolean isValid(Set<String> mailTypeParameters) {
        if (paramaters.size() != mailTypeParameters.size()) {
            return false;
        }
        mailTypeParameters.removeAll(paramaters.keySet());
        if (!mailTypeParameters.isEmpty()) {
            return false;
        }
        return true;
    }
}
