package com.emrah.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * created by emrahkucukkaraca on 14.09.2018
 **/
@Documented @Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidMailParameterValidator.class)
public @interface ValidMailParameter {

    String message() default "{validation.mail.ValidMailParameter.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
