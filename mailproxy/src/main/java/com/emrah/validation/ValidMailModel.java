package com.emrah.validation;

import java.util.Set;

/**
 * created by emrahkucukkaraca on 14.09.2018
 **/
public interface ValidMailModel {

    boolean isValid(Set<String> parameters);

    String getMailTypeCode();

}
