package com.emrah.validation;

import com.emrah.mailacceptor.control.MailTypeRepository;
import com.emrah.mailacceptor.entity.MailType;

import javax.inject.Inject;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * created by emrahkucukkaraca on 14.09.2018
 **/
public class ValidMailParameterValidator
    implements ConstraintValidator<ValidMailParameter, ValidMailModel> {


    @Inject
    MailTypeRepository mailTypeRepository;

    @Override
    public boolean isValid(final ValidMailModel validMailModel,
        final ConstraintValidatorContext constraintValidatorContext) {
        MailType byCode = mailTypeRepository.findByCode(validMailModel.getMailTypeCode());
        if (byCode == null) {
            return false;
        }
        return validMailModel.isValid(byCode.getParameters());

    }

    @Override public void initialize(final ValidMailParameter constraintAnnotation) {

        //nothing to do here
    }



}
