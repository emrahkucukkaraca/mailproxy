package com.emrah.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * created by emrahkucukkaraca on 12.09.2018
 **/
public class ValidDateTimeValidator implements ConstraintValidator<ValidDateTime, String> {

    private final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private ValidDateTime constraintAnnotation;

    @Override
    public void initialize(ValidDateTime constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return validator(constraintAnnotation.min(), constraintAnnotation.max(), value);

    }

    boolean validator(String minDate, String maxDate, String value) {
        try {
            if (value == null) { // there is no Schedule
                return true;
            }
            final Date min = dateParser.parse(minDate);
            final Date max = dateParser.parse(maxDate);
            final Date date = dateParser.parse(value);
            if (date.after(min) && date.before(max)) {
                return true;
            }
            return false;
        } catch (ParseException ex) {
            return false;
        }
    }


}
