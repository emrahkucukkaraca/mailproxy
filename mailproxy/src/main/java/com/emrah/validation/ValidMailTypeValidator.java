package com.emrah.validation;

import com.emrah.mailacceptor.control.MailTypeRepository;
import com.emrah.mailacceptor.entity.MailType;

import javax.inject.Inject;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * created by emrahkucukkaraca on 12.09.2018
 **/
public class ValidMailTypeValidator implements ConstraintValidator<ValidMailType, String> {

    @Inject
    MailTypeRepository mailTypeRepository;

    @Override
    public void initialize(final ValidMailType constraintAnnotation) {
        //nothing to do here
    }


    @Override
    public boolean isValid(final String s,
        final ConstraintValidatorContext constraintValidatorContext) {
        MailType byCode = mailTypeRepository.findByCode(s);
        if (byCode != null) {
            return true;
        }
        return false;
    }
}
