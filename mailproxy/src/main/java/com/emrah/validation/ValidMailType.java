package com.emrah.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by emrahkucukkaraca on 11.09.2018.
 */

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = ValidMailTypeValidator.class)
@Documented
public @interface ValidMailType {

    String message() default "{validation.mail.ValidMailType.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
