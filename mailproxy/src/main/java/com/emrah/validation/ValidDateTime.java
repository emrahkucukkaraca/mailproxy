package com.emrah.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * created by emrahkucukkaraca on 12.09.2018
 **/


@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = ValidDateTimeValidator.class)
@Documented
public @interface ValidDateTime {

    String message() default "{validation.date.ValidDateTime.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


    String min() default "2018-09-12 00:00";

    String max() default "2999-12-31 00:00";

}
