package com.emrah.mailsender.control;

import com.emrah.mailsender.entity.ValidMail;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by emrahkucukkaraca on 15.09.2018.
 */
@Interceptors(HealthMeter.class)
public class CDCProvider implements MailProvider {

    @Inject
    private ErrorProvider errorProvider;

    private Logger logger = Logger.getLogger(CDCProvider.class.getName());



    @Override
    @HealthMetered(slowerThanMillis = 20)
    public Boolean sendMail(ValidMail validMail) {

        //some logic for sending mail from different provider
        // for the test purposes randomly generating error or slowing down the method
        logger.log(Level.INFO, "mail send");
        return errorProvider.getErrorOrNot();



    }

    @Override public String getName() {
        return "CDCProvider";
    }



}
