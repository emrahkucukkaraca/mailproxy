package com.emrah.mailsender.control;

import com.emrah.mailsender.entity.MailStatus;
import com.emrah.mailsender.entity.ValidMail;

import javax.ejb.DependsOn;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by emrahkucukkaraca on 11.09.2018.
 */
@Singleton
@Startup
@DependsOn("MailProviderHealth")
public class SendMail {

    private final static int MAX_RETRY_COUNT=3;
    @Inject
    ValidMailRepository validMailRepository;
    @Inject
    MailProviderHealth mailProviderHealth;

    @Schedule(hour = "*", minute = "*",persistent = false)
    public void sendMails() {
        List<ValidMail> unSendMails = validMailRepository.findUnSendMails(1000);
        unSendMails.forEach(this::processMail);
    }

    private void processMail(ValidMail validMail) {
        Short retryCount = validMail.getRetryCount();
        validMail.setRetryCount((short) (retryCount+1));
        boolean send = sendMailFromAvailableSender(validMail);
        if (send) {
            validMail.setStatus(MailStatus.SEND);
        } else {
            validMail.setStatus(findMailStatus(retryCount));
        }
        validMailRepository.save(validMail);
    }

    private boolean sendMailFromAvailableSender(ValidMail validMail) {
        try {
            return mailProviderHealth.getHealthyProvider().sendMail(validMail);
        } catch (Exception e) {
            return false;
        }
    }

    private MailStatus findMailStatus(Short retryCount) {
        if (retryCount >= MAX_RETRY_COUNT) {
            return MailStatus.FAILED;
        }
        return MailStatus.RETRY;
    }


}
