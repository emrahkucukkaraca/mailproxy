package com.emrah.mailsender.control;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by emrahkucukkaraca on 17.09.2018.
 */

@Singleton
@Startup
public class MailProviderHealth {


    private static final int MAX_FAILURES_COUNT = 5;
    @Inject
    Instance<MailProvider> providers;
    private Map<String, Integer> errorMap = new HashMap<>();
    private Map<String, Boolean> healthMap = new HashMap<>();

    @PostConstruct
    public void init() {

        providers.forEach(mailProvider -> {
            errorMap.put(mailProvider.getName(), 0);
            healthMap.put(mailProvider.getName(), true);
        });

    }

    public void observeErrors(@Observes String clazz) {
        Integer errorCount = errorMap.get(clazz);
        System.out.println(errorCount);
        healthMap.put(clazz, (errorCount + 1) < MAX_FAILURES_COUNT);
        errorMap.put(clazz, errorCount + 1);
        healthMap.keySet().forEach(k-> System.out.println(k+"-->"+healthMap.get(k)));
        errorMap.keySet().forEach(k-> System.out.println(k+"-->"+errorMap.get(k)));
    }

    public MailProvider getHealthyProvider(){
        List<MailProvider> collect = providers.stream().filter(provider -> healthMap.get(provider.getName())).collect(Collectors.toList());
        if(!collect.isEmpty()){
            return collect.get(0);
        }
        return null;
    }

}
