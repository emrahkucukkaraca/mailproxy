package com.emrah.mailsender.control;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 * Created by emrahkucukkaraca on 17.09.2018.
 */
public class HealthMeter {

    private static final int TIMEOUT_IN_MS = 1000;

    @Inject
    Event<String> event;

    @AroundInvoke
    public Object guard(InvocationContext ic) throws Exception {

        long timeout = TIMEOUT_IN_MS;
        HealthMetered configuration = ic.getMethod().getAnnotation(HealthMetered.class);

        if (configuration != null) {
            timeout = configuration.slowerThanMillis();
        }
        long start = System.currentTimeMillis();
        try {
            return ic.proceed();
        } catch (Exception ex) {
            fireEvent(ic);
            throw ex;
        } finally {
            long duration = System.currentTimeMillis() - start;
            if (duration >= timeout) {
                fireEvent(ic);
            }
        }
    }

    protected void fireEvent(InvocationContext ic) {

        event.fire(ic.getMethod().getDeclaringClass().getSimpleName());
    }
}
