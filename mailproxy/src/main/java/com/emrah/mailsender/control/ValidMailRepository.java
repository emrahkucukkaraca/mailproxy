package com.emrah.mailsender.control;

import com.emrah.mailsender.entity.ValidMail;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.List;

/**
 * created by emrahkucukkaraca on 14.09.2018
 **/

@Stateless
public class ValidMailRepository {

    @PersistenceContext
    EntityManager entityManager;

    public ValidMail save(ValidMail validMail) {
        return entityManager.merge(validMail);

    }

    public ValidMail findById(final Long id) {
        return entityManager.find(ValidMail.class, id);
    }

    public List<ValidMail> findUnSendMails(final int mailCount) {
        return entityManager.createNamedQuery(ValidMail.FIND_UNSEND_MAILS, ValidMail.class).setParameter("time", LocalDateTime.now()).setMaxResults(mailCount).getResultList();
    }
}
