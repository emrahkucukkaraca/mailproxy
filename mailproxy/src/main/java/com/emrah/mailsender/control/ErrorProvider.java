package com.emrah.mailsender.control;


import java.util.Random;

/**
 * Created by emrahkucukkaraca on 18.09.2018.
 */
public class ErrorProvider {

    public Boolean getErrorOrNot()  {
        Random rand = new Random();
        float v = rand.nextFloat();
        if (v < 0.50f) {

            return true;
        } else if (v < 0.7f) {
            try {
                Thread.sleep(100);
                return true;
            } catch (InterruptedException e) {
            }
        }
        throw new RuntimeException("Random provider exception");
    }
}
