package com.emrah.mailsender.control;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by emrahkucukkaraca on 17.09.2018.
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface HealthMetered {

    long slowerThanMillis() default 1000;
}
