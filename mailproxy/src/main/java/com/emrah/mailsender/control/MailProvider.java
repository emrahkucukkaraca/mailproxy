package com.emrah.mailsender.control;

import com.emrah.mailsender.entity.ValidMail;

/**
 * Created by emrahkucukkaraca on 9.09.2018.
 */
public interface MailProvider {

    Boolean sendMail(ValidMail validMail);

    String getName();



}
