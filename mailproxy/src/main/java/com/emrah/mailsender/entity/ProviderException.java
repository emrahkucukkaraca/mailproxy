package com.emrah.mailsender.entity;

/**
 * Created by emrahkucukkaraca on 18.09.2018.
 */
public class ProviderException extends Exception{

    public ProviderException(String message) {
        super(message);
    }
}
