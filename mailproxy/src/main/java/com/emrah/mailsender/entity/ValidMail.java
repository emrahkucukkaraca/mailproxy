package com.emrah.mailsender.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

import static com.emrah.mailsender.entity.ValidMail.FIND_UNSEND_MAILS;

/**
 * Created by emrahkucukkaraca on 11.09.2018.
 */
@Entity
@Table(name = "VALID_MAILS")
@NamedQuery(name = FIND_UNSEND_MAILS,query = "SELECT vm FROM ValidMail vm WHERE (vm.timeToSend is null or vm.timeToSend<=:time) and (vm.status='NOT_SEND' or (vm.status='RETRY' and vm.retryCount<=3 )) ")
public class ValidMail {


    public static final String PREFIX = "MailType";
    public static final String FIND_UNSEND_MAILS = PREFIX + ".FindUnSendMails";
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "from_mail")
    private String from;
    @Column(name = "to_mail")
    private String to;
    private String subject;
    private String body;
    @Column(name = "time_to_send")
    private LocalDateTime timeToSend;
    @Enumerated(EnumType.STRING)
    private MailStatus status;
    @Column(name = "retry_count")
    private Short retryCount;



    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(final String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(final String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(final String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(final String body) {
        this.body = body;
    }

    public LocalDateTime getTimeToSend() {
        return timeToSend;
    }

    public void setTimeToSend(final LocalDateTime timeToSend) {
        this.timeToSend = timeToSend;
    }

    public MailStatus getStatus() {
        return status;
    }

    public void setStatus(final MailStatus status) {
        this.status = status;
    }

    public Short getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(final Short retryCount) {
        this.retryCount = retryCount;
    }


}
