package com.emrah.mailsender.entity;

/**
 * Created by emrahkucukkaraca on 11.09.2018.
 */
public enum MailStatus {
    SEND, NOT_SEND, RETRY, FAILED
}
