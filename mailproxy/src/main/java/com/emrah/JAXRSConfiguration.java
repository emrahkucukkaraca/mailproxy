package com.emrah;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by emrahkucukkaraca on 9.09.2018.
 */
@ApplicationPath("resources") public class JAXRSConfiguration extends Application {

}
