package com.emrah.mailacceptor.entity;

import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * created by emrahkucukkaraca on  14.09.2018
 **/
public class MailTest {

    @Test
    public void validMail() {
       Mail mail = new Mail();
        Map<String, String> parameters = new HashMap<>();
        parameters.put("TEST1", "value1");
        parameters.put("TEST2", "value2");
        mail.setParamaters(parameters);
        Set<String> params = new HashSet<>();
        params.add("TEST1");
        params.add("TEST2");
        assertTrue(mail.isValid(params));
    }

    @Test
    public void invalidMailwithFewerParams() {
       Mail mail = new Mail();
        Map<String, String> parameters = new HashMap<>();
        parameters.put("TEST2", "value2");
        mail.setParamaters(parameters);
        Set<String> params = new HashSet<>();
        params.add("TEST1");
        params.add("TEST2");
        assertFalse(mail.isValid(params));
    }

    @Test
    public void invalidMailwithDifferentParams() {
        Mail mail = new Mail();
        Map<String, String> parameters = new HashMap<>();
        parameters.put("TEST11", "value1");
        parameters.put("TEST2", "value2");
        mail.setParamaters(parameters);
        Set<String> params = new HashSet<>();
        params.add("TEST1");
        params.add("TEST2");
        assertFalse(mail.isValid(params));
    }
}
