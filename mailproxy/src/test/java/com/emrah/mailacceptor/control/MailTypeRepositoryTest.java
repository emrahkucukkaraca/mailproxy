package com.emrah.mailacceptor.control;

import com.emrah.mailacceptor.entity.MailType;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;

/**
 * created by emrahkucukkaraca on 14.09.2018
 **/
public class MailTypeRepositoryTest {

    private final static String CODE = "CODE";
    private EntityManager em;
    private EntityTransaction tx;
    private MailTypeRepository mailTypeRepository;

    @Before
    public void init() {
        mailTypeRepository = new MailTypeRepository();
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("it");
        this.em = entityManagerFactory.createEntityManager();
        mailTypeRepository.entityManager = this.em;
        this.tx = this.em.getTransaction();
    }

    @Test
    public void findAll() {
       clean();
        List<MailType> all = mailTypeRepository.findAll();
        assertTrue(all != null);
        save();
        List<MailType> all1 = mailTypeRepository.findAll();
        assertFalse(all1.isEmpty());
    }

    private void clean() {
        tx.begin();
        em.createQuery("DELETE FROM MailType mt").executeUpdate();
        tx.commit();
    }

    @Test
    public void findByCode() {
        clean();
        MailType byCode = mailTypeRepository.findByCode(CODE);
        assertNull(byCode);
        save();
        MailType saved = mailTypeRepository.findByCode(CODE);
        assertNotNull(saved);
        assertEquals(saved.getCode(), CODE);

    }


    @Test
    public void save() {
        clean();
        MailType mailType = getMailType();
        this.tx.begin();
        MailType save = mailTypeRepository.save(mailType);
        this.tx.commit();
        assertNotNull(save.getId());
        assertTrue(save.getCode().equalsIgnoreCase(mailType.getCode()));

    }

    private MailType getMailType() {
        MailType mailType = new MailType();
        mailType.setCode(CODE);
        mailType.setFrom("abc@abc.com");
        mailType.setName("NAME");
        mailType.setParameters(new HashSet<>());
        mailType.getParameters().add("PARAM1");
        mailType.getParameters().add("PARAM2");
        mailType.setSubject("SUBJECT");
        mailType.setTemplate("TEMPLATE");
        return mailType;
    }
}
