package com.emrah.validation;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * created by emrahkucukkaraca on 12:09 - 14.09.2018
 **/
public class ValidDateTimeValidatorTest {

    @Test
    public void validDates() {
        ValidDateTimeValidator validDateTimeValidator = new ValidDateTimeValidator();
        assertTrue(validDateTimeValidator.validator("2018-09-12 00:00", "2999-12-31 00:00", "2018-09-13 00:00"));
    }

    @Test
    public void inValidDates() {
        ValidDateTimeValidator validDateTimeValidator = new ValidDateTimeValidator();
        assertFalse(validDateTimeValidator.validator("2018-09-12 00:00", "2999-12-31 00:00", "2018-09-10 00:00"));
    }


    @Test
    public void inValidDateFormat() {
        ValidDateTimeValidator validDateTimeValidator = new ValidDateTimeValidator();
        assertFalse(validDateTimeValidator.validator("2018-09-12 00:00", "2999-12-31 00:00", "2018-09-10"));
    }
}
