package com.emrah.mailproxy.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by emrahkucukkaraca on 9.09.2018.
 */
public class Mail implements Serializable {

    private String mailTypeCode;
    private String to;
    private Map<String, String> paramaters = new HashMap<>();
    private String timeToSend;


    public String getMailTypeCode() {
        return mailTypeCode;
    }

    public void setMailTypeCode(String mailTypeCode) {
        this.mailTypeCode = mailTypeCode;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Map<String, String> getParamaters() {
        return paramaters;
    }

    public void setParamaters(Map<String, String> paramaters) {
        this.paramaters = paramaters;
    }

    public String getTimeToSend() {
        return timeToSend;
    }

    public void setTimeToSend(final String timeToSend) {
        this.timeToSend = timeToSend;
    }
}
