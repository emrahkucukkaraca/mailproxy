package com.emrah.mailproxy.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by emrahkucukkaraca on 9.09.2018.
 */
public class MailType implements Serializable {

    private Long id;
    private String code;
    private String name;
    private String from;
    private String subject;
    private String template;
    private Set<String> parameters = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Set<String> getParameters() {
        return parameters;
    }

    public void setParameters(final Set<String> parameters) {
        this.parameters = parameters;
    }
}
