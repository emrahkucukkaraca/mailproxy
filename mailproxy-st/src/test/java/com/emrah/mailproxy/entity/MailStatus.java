package com.emrah.mailproxy.entity;

/**
 * Created by emrahkucukkaraca on 11.09.2018.
 */
public enum MailStatus {
    SEND, NOTSEND, RETRY
}
