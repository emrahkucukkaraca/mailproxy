package com.emrah.mailproxy;

import com.emrah.mailproxy.entity.MailType;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * created by emrahkucukkaraca on 12.09.2018
 **/
public class MailTypeResourceIT {

    private Client restClient;
    private WebTarget target;
    private Gson gson;

    @Before
    public void init() {
        restClient = ClientBuilder.newClient();
        this.target = restClient.target("http://localhost:8080/mailproxy/resources/mail-types");
        gson= new Gson();
    }


    public void createMailType(MailType mailType) {
        Response post = null;
        try {
            post = target.request().post(Entity.json(gson.toJson(mailType,MailType.class)));
            assertEquals(post.getStatus(), Response.Status.CREATED.getStatusCode());
            String location = post.getHeaderString("Location");
            assertNotNull(location);
            readMailType(location,mailType);
        } finally {
            if (post != null) {
                post.close();
            }
        }
    }

    public void readMailType(String location, MailType mailType) {
        Response response = null;
        try {
            response = restClient.target(location).request().get();
            assertEquals(response.getStatus(), Response.Status.OK.getStatusCode());
            MailType result = gson.fromJson(response.readEntity(String.class),MailType.class);
            assertNotNull(result.getId());
            assertEquals(result.getCode(), mailType.getCode());
            assertNotNull(result.getParameters());
            assertFalse(result.getParameters().isEmpty());
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }


    @Test
    public void mailTypeResourceTest() {

        MailType orderMail = new MailType();
        orderMail.setCode("OrderMail");
        orderMail.setFrom("emrahkucukkaraca@gmail.com");
        orderMail.setName("Order Mail");
        orderMail.setSubject("##NAME## Your order is received");
        orderMail.setTemplate("Hi ##NAME##, your order with the id ##ORDERID## is received.");
        orderMail.setParameters(new HashSet<>());
        orderMail.getParameters().add("##NAME##");
        orderMail.getParameters().add("##ORDERID##");
        createMailType(orderMail);

        MailType lostPasswordMail = new MailType();
        lostPasswordMail.setCode("LostPasswordMail");
        lostPasswordMail.setFrom("emrahkucukkaraca@gmail.com");
        lostPasswordMail.setName("Lost Password Mail");
        lostPasswordMail.setSubject("##NAME## Your password recovery link is here");
        lostPasswordMail.setTemplate("Hi ##NAME##, Your password recovery link is here. ##LINK##");
        lostPasswordMail.setParameters(new HashSet<>());
        lostPasswordMail.getParameters().add("##NAME##");
        lostPasswordMail.getParameters().add("##LINK##");
        createMailType(lostPasswordMail);

        MailType shipmentMail = new MailType();
        shipmentMail.setCode("ShipmentMail");
        shipmentMail.setFrom("emrahkucukkaraca@gmail.com");
        shipmentMail.setName("Shipment Mail");
        shipmentMail.setSubject("##NAME## Your order is send");
        shipmentMail.setTemplate("Hi ##NAME##, Your order with the id ##ORDERID## is ready and on the way.");
        shipmentMail.setParameters(new HashSet<>());
        shipmentMail.getParameters().add("##NAME##");
        shipmentMail.getParameters().add("##ORDERID##");
        createMailType(shipmentMail);

        String result = target.request().get().readEntity(String.class);
        MailType[] mailTypes = gson.fromJson(result, MailType[].class);
        assertTrue(mailTypes.length >= 3);

        Response post = null;
        try {
            post = target.request().post(Entity.json(gson.toJson(orderMail)));
            assertEquals(post.getStatus(), Response.Status.BAD_REQUEST.getStatusCode());

        } finally {
            if (post != null) {
                post.close();
            }
        }
    }


}
