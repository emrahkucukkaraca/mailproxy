package com.emrah.mailproxy;

import com.emrah.mailproxy.entity.Mail;
import com.emrah.mailproxy.entity.MailType;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * created by emrahkucukkaraca on 13.09.2018
 **/
public class MailSendResourceIT {

    private Client restClient;
    private WebTarget target;
    private Gson gson;

    @Before public void init() {


        restClient = ClientBuilder.newClient();
        this.target = restClient.target("http://localhost:8080/mailproxy/resources/mails");
        // check mail type exist
        List<MailType> mailTypes = restClient.target("http://localhost:8080/mailproxy/resources/mail-types").request().get()
            .readEntity(new GenericType<List<MailType>>() {
            });
        if(mailTypes.isEmpty()){// run other test to fill mail types
            MailTypeResourceIT mailTypeResourceIT= new MailTypeResourceIT();
            mailTypeResourceIT.init();
            mailTypeResourceIT.mailTypeResourceTest();

        }

    }

    @Test
    public void sendMailValidationTest() {
        Mail mail = new Mail();
        mail.setMailTypeCode("code1232321");
        Map<String, String> parameters = new HashMap<>();
        parameters.put("##PARAM1##", "Emrah");
        parameters.put("##PARAM2##", "Hasan");
        mail.setParamaters(parameters);
        mail.setTo("emrahkucukkaraca@gmail.com");
        mail.setTimeToSend("2018-09-15 00:00:00");
        Response post = target.request().post(Entity.json(mail));
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),post.getStatus());
    }

    @Test
    public void sendMailTest(){
        for (int i = 0; i <50 ; i++) {
            Mail mail = new Mail();
            mail.setMailTypeCode("LostPasswordMail");
            mail.setTo("emrahkucukkaraca@gmail.com");
            mail.setParamaters(new HashMap<>());
            mail.getParamaters().put("##NAME##","Emrah");
            mail.getParamaters().put("##LINK##","http://test.test.com");
            mail.setTimeToSend("2018-09-18 06:01");
            Response post = target.request().post(Entity.json(mail));
            assertEquals(Response.Status.OK.getStatusCode(),post.getStatus());
            post.close();
        }




    }

}
